<?php if(!isset($is_news_home) || $is_news_home == false) { ?>
<a class="action_button" href="<?php bloginfo('url'); ?>/original-posts/" style="display: block; text-align: center; margin-bottom: 20px;"><?php _e('Read the archives...', 'grass'); ?></a>
<?php } ?>

<div class="subtle_box">
    <h4><?php _e('Connect with GNOME', 'grass'); ?></h4>
    
    <div class="social_network_icons">
        <ul>
            <li>
                <a href="http://identi.ca/gnome">
                    <img src="<?php bloginfo('template_url')?>/images/social_networks/identica.png" alt=" " />
                    Identi.ca
                </a>
            </li>
            <li>
                <a href="http://twitter.com/gnome">
                    <img src="<?php bloginfo('template_url')?>/images/social_networks/twitter.png" alt=" " />
                    Twitter
                </a>
            </li>
            <li>
                <a href="http://www.facebook.com/GNOMEDesktop">
                    <img src="<?php bloginfo('template_url')?>/images/social_networks/facebook.png" alt=" " />
                    Facebook
                </a>
            </li>
        </ul>
    </div>
</div>
    
<div class="subtle_box" style="display: block; text-align: center; margin-bottom: 20px;">
    <h4>Visit Planet GNOME</h4>
    <p>For inspiring, in-depth blog posts from all kinds of people involved in the GNOME Community. <a href="http://planet.gnome.org"><img src="<?php bloginfo('template_url')?>/images/planet-logo_small.png" /></a></p>
</div>
